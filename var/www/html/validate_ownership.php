<?php


$date = date("d/m/Y");
$fname = $_POST['fname'];
$lname = $_POST['lname'];
$dob = $_POST['dob'];
$add1 = $_POST['add1'];
$add2 = $_POST['add2'];
$suburb = $_POST['suburb'];
$state = $_POST['state'];
$Email = $_POST['Email'];
$phone = $_POST['phone'];
$model = $_POST['model'];
$vin = $_POST['vin'];
$regostate = $_POST['regostate'];
$regoplate = $_POST['regoplate'];
$captcha= $_POST['g-recaptcha-response'];

$to      = $Email;

$subject = 'Change of Ownership Request';

$message = "<html><body>";
$message .= "<p>This is a copy of the Change of Ownership you have submitted to Moto Guzzi Australia.</p>" . "<p>Our Customer Care Team will be in contact with you shortly. Please keep this email for your record.</p>";
$message .= "Date: " . $date . "<br>" ."<br>";
$message .= '<table>';
$message .= "<tr><td><strong>First Name:</strong> </td><td>" . $fname . "</td></tr>";
$message .= "<tr><td><strong>Last Name:</strong> </td><td>" . $lname . "</td></tr>";
$message .= "<tr><td><strong>D.O.B:</strong> </td><td>" . $dob . "</td></tr>";
$message .= "<tr><td><strong>Address 1:</strong> </td><td>" . $add1 . "</td></tr>";
$message .= "<tr><td><strong>Address 2:</strong> </td><td>" . $add2 . "</td></tr>";
$message .= "<tr><td><strong>Suburb:</strong> </td><td>" . $suburb . "</td></tr>";
$message .= "<tr><td><strong>State:</strong> </td><td>" . $state . "</td></tr>";
$message .= "<tr><td><strong>Email:</strong> </td><td>" . $Email . "</td></tr>";
$message .= "<tr><td><strong>Phone:</strong> </td><td>" . $phone . "</td></tr>";
$message .= "<tr><td><strong>Model:</strong> </td><td>" . $model . "</td></tr>";
$message .= "<tr><td><strong>Vin Number:</strong> </td><td>" . $vin . "</td></tr>";
$message .= "<tr><td><strong>Registration State:</strong> </td><td>" . $regostate . "</td></tr>";
$message .= "<tr><td><strong>Registration Plate:</strong> </td><td>" . $regoplate . "</td></tr>";
$message .= "</table>";
$message .= "</body></html>";
$message .= "<p><br/><br/>Kind Regards, <br/>Moto Guzzi Australia</p>";


// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: John Sample Automotive <customercare@motoguzzi.com.au>' . "\r\n" . 'Reply-To: ' . $Email . "\r\n" . 'BCC: customercare@motoguzzi.com.au';
			
					
// Storing in myphpAdmin Databse

if( !empty($fname) and !empty($lname) and !empty($dob) and !empty($add1) and !empty($suburb) and !empty($state) and !empty($Email) and !empty($phone) and !empty($model) and !empty($vin) and !empty($captcha)){
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf4ARMTAAAAABPHnDPGO89AEmTfFXsTqJ2LkEjh&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	
	mail($to, $subject, $message, $headers);
	
	header("Location: success_ownership.php");
		
} 

else{
	header("Location: error_ownership.php");	
}
mysql_close();
?>