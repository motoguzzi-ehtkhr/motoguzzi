
<div class="pgl-shortcodes">
	<ul class="wrapper clearfix">
		<?php foreach( $shortcodes as $key => $shortcode ){ ?>
		<li class="shortcode-col">
			<div class="pgl-shorcode-button btn btn-default" data-name="<?php echo $shortcode['name'];?>">
				<div class="content">
					<div class="title"><?php echo $shortcode['title'] ?></div>
					<em><?php echo $shortcode['desc'] ?></em>
				</div>
			</div>
		</li>
		<?php  } ?>
	</ul>
</div>
<script>
jQuery(".pgl-shortcodes").PGL_Shortcode();
</script>