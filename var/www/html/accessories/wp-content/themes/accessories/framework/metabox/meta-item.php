<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {
	global $theme_option;

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_pgl_';
	$footers_type = get_posts( array('posts_per_page'=>-1,'post_type'=>'footer') );
    $footers_option = array();
    $footers_option['global'] = 'Use Global';
    foreach ($footers_type as $key => $value) {
        $footers_option[$value->ID] = $value->post_title;
    }

    $page_configs = array();
    $page_configs[] = array(
		'name' => __( 'Layout', 'accessories' ),
		'desc' => __( 'Select Layout', 'accessories' ),
		'id'   => $prefix . 'page_layout',
		'type' => 'layout',
		'default' => '1'
	);
    
    if( in_array( 'revslider/revslider.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )){
    	global $wpdb;
		$rs = $wpdb->get_results(
			"
SELECT id, title, alias
FROM " . $wpdb->prefix . "revslider_sliders
ORDER BY id ASC LIMIT 999
"
		);
		$revsliders = array(''=>'----- Select -----');
		if ( $rs ) {
			foreach ( $rs as $slider ) {
				$revsliders[$slider->alias] = $slider->title;
			}
		} 

		$page_configs[] = array(
				'name'    => __( 'Revolution Slider Top', 'accessories' ),
				'desc'    => 'Select your Revolution Slider.',
				'id'      => $prefix . 'revslider',
				'type'    => 'select',
				'options' => $revsliders,
				'std' => ''
			);
    }

    $page_configs[] = array(
				'name' => __( 'Left Sidebar', 'accessories' ),
				'id'   => $prefix . 'page_left_sidebar',
				'type' => 'sidebar',
			);

    $page_configs[] = array(
				'name' => __( 'Right Sidebar', 'accessories' ),
				'id'   => $prefix . 'page_right_sidebar',
				'type' => 'sidebar',
			);
    $page_configs[] = array(
				'name' => __( 'Show Breadcrumb', 'accessories' ),
				'id'   => $prefix . 'show_breadcrumb',
				'type' => 'button_radio',
			);
    $page_configs[] = array(
				'name' => __( 'Show Title', 'accessories' ),
				'id'   => $prefix . 'show_title',
				'type' => 'button_radio',
				'std'  => false
			);
    $page_configs[] = array(
				'name' => __( 'Blog pages show at most', 'accessories' ),
				'id'   => $prefix . 'blog_count',
				'type' => 'text_number',
				'std'  => 6
			);
    $page_configs[] = array(
				'name'    => __( 'Blog Skin', 'accessories' ),
				'id'      => $prefix . 'blog_skin',
				'type'    => 'select',
				'options' => array(
					'default' => 'Blog default',
					'mini' 	  => 'Blog mini sidebar',
					'masonry' 	  => 'Blog masonry',
				),
				'std' => 'global'
			);
    $page_configs[] = array(
				'name' => __( 'Blog Masonry column count', 'accessories' ),
				'id'   => $prefix . 'blog_masonry_column_count',
				'type' => 'text_number',
				'std'  => 3
			);
    $page_configs[] = array(
				'name' => __( 'Override Theme Options', 'accessories' ),
				'id'   => $prefix . 'override_options',
				'type' => 'title',
			);
   //  $page_configs[] = array(
			//     'name' => 'Override Logo',
			//     'desc' => 'Upload an image or enter an URL.',
			//     'id' => $prefix . 'logo_override',
			//     'type' => 'file',
			//     'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
			// );
    $page_configs[] = array(
				'name'    => __( 'Header Style', 'accessories' ),
				'id'      => $prefix . 'header_style',
				'type'    => 'select',
				'options' => array(
					'global' => __( 'Use Global', 'accessories' ),
					'1'   => __( 'Style 1', 'accessories' ),
					'2'     => __( 'Style 2', 'accessories' ),
					// '3'     => __( 'Style 3', 'accessories' ),
					// '4'     => __( 'Style 4', 'accessories' ),
				),
				'std' => 'global'
			);
    $page_configs[] = array(
				'name'    => __( 'Footer Style', 'accessories' ),
				'id'      => $prefix . 'footer_style',
				'type'    => 'select',
				'options' => $footers_option,
				'std' => 'global'
			);
	$meta_boxes['page_config'] = array(
		'id'         => 'page_config',
		'title'      => __( 'Page Config', 'accessories' ),
		'pages'      => array( 'page' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => $page_configs
	);

	$meta_boxes['post_config'] = array(
		'id'         => 'post_config',
		'title'      => __( 'Post Config', 'accessories' ),
		'pages'      => array( 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(
			array(
				'name' => __( 'Link Video or Audio', 'accessories' ),
				'desc' => __( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'accessories' ),
				'id'   => $prefix . 'post_video',
				'type' => 'oembed',
			),
			array(
			    'name' => 'Gallery Images',
			    'desc' => '',
			    'id' => $prefix . 'post_gallery',
			    'type' => 'file_list',
			    // 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
			),
			array(
			    'name' => 'Status',
			    'desc' => '',
			    'id' => $prefix . 'post_status',
			    'type' => 'textarea',
			),
		)
	);

	if( $theme_option['is-seo'] ){
		$meta_boxes['seo_meta'] = array(
			'id'         => 'seo_meta',
			'title'      => __( 'Seo Options', 'accessories' ),
			'pages'      => array( 'page','post' ), // Post type
			'context'    => 'normal',
			'priority'   => 'high',
			'show_names' => true, // Show field names on the left
			// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
			'fields'     => array(
				array(
					'name'       => __( 'Title', 'accessories' ),
					'id'         => $prefix . 'seo_title',
					'type'       => 'text',
					'show_on_cb' => 'cmb_test_text_show_on_cb', // function should return a bool value
					
				),
				array(
					'name'       => __( 'Keywords', 'accessories' ),
					'id'         => $prefix . 'seo_keywords',
					'type'       => 'text',
				),
				array(
					'name' => __( 'Description', 'accessories' ),
					'id'   => $prefix . 'seo_description',
					'type' => 'textarea',
				),
			)
		);
	}

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) ){
		get_template_part( 'framework/metabox/init' );
		get_template_part( 'framework/metabox/meta-custom' );
	}
}
