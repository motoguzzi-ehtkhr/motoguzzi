<?php
$my_theme = wp_get_theme();
define( 'PGL_THEME_VERSION', $my_theme->get( 'Version' ) );

require( 'framework/init.php');


//Adding SKU in product catalog page 
add_action( 'woocommerce_after_shop_loop_item', 'custom_after_shop_loop_item', 5);
function custom_after_shop_loop_item() {
  global $post, $product;

  echo '<p>Part No.: '.$product->get_sku().'</p>';
}


//Adding Short Description in product catalog page 
add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_single_excerpt', 5);

//Redirect to shop after logging out from admin
add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}