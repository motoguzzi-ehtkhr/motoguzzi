<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<div class="pgl-search">
		<input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Search', 'accessories' ); ?>" />
		<i class="fa fa-search"></i>
	</div>
	<?php if(PLG_WOOCOMMERCE_ACTIVED){ ?>
		<input type="hidden" name="post_type" value="product" />
	<?php } ?>
</form>