<?php 
    global $theme_option,$woocommerce;
    $login_url = wp_login_url();
    $myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
    if ( $myaccount_page_id ) {
        $login_url = get_permalink( $myaccount_page_id );
        if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
            $login_url = str_replace( 'http:', 'https:', $login_url );
        }
    }
?>
<header id="pgl-header" class="pgl-header">
    <?php if(isset($theme_option['header-is-topbar']) && $theme_option['header-is-topbar']){ ?>
    <div id="header-topbar">
        <div class="container">
            <div class="inner-topbar row">
                <?php if (!is_user_logged_in()) { ?>
                <div class="col-sm-6 hidden-xs"><?php _e('Welcome to our store. Please','accessories'); ?> <a href="<?php echo esc_url( $login_url ); ?>"><?php _e('Login','accessories'); ?></a> or <a href="<?php echo esc_url( $register_url ); ?>"><?php _e('Register','accessories'); ?></a></div>
                <?php }else{ ?>
                <?php 
                    $current_user = wp_get_current_user();
                ?>
                <div class="col-sm-6 hidden-xs">
                    <?php echo __('Welcome','accessories'). ' ' . $current_user->user_login ; ?>!
                </div>
                <?php } ?>
                <?php if($theme_option['header-is-switch-language']){ ?>
                <div class="col-sm-6">
                    <div class="language-filter pull-right">
                        <?php echo __('Language','accessories'). ': ' . pgl_language_flags(); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="header-content">
        <div class="container">
            <div class="header-content-inner">
                <div class="row">
                    <div class="col-md-3 logo">
                        <a href="http://www.motoguzzi.com.au/home.html">
                            <img src="http://www.motoguzzi.com.au/accessories/wp-content/uploads/2015/12/motoguzzilogo.jpg" alt="MG Accessories">
                        </a>
                    </div>
                    <div class="col-md-9 content-navigation">
                        <?php if($theme_option['header-is-search']){ ?>
                        <div class="search-form clearfix">
                            <div class="pull-right">
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <div class="toggle-menu">
                            <a href="javascript:;" class="off-canvas-toggle icon-toggle" data-uk-offcanvas="{target:'#pgl-off-canvas'}">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
                        <?php if(PLG_WOOCOMMERCE_ACTIVED && $theme_option['header-is-cart']){ ?>
                        <div class="pull-right shoppingcart">
                            <a href="javascript:;" data-uk-offcanvas="{target:'#pgl_cart_canvas'}">
                                <?php echo __('My Cart','accessories'); ?> <span class="count">(<?php echo $woocommerce->cart->cart_contents_count; ?>)</span>
                            </a>
                        </div>
                        <?php } ?>
                        <?php pgl_megamenu(array(
                            'theme_location' => 'mainmenu',
                            'container_class' => 'collapse navbar-collapse navbar-ex1-collapse pull-right',
                            'menu_class' => 'nav navbar-nav megamenu',
                            'show_toggle' => false
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php do_action('pgl_after_header'); ?>
</header>
<!-- //HEADER -->