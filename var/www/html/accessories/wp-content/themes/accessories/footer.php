<?php

global $theme_option;
?>
        	<footer id="pgl-footer" class="pgl-footer">
                <?php 
                   do_action( 'pgl_footer_layout_style' );
                ?>
                <div class="footer-copyright">
				    <div class="container">
				        <?php echo $theme_option['footer-copyright']; ?>
				    </div>
				</div>
        	</footer>
        </div><!--  End .wrapper-inner -->
    </div><!--  End .pgl-wrapper -->
    
    <?php do_action('pgl_after_wrapper'); ?>

	<?php wp_footer(); ?>
</body>
</html>