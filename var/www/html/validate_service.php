<?php


$date = date("d/m/Y");
$name = $_POST['name'];
$Email = $_POST['Email'];
$phone = $_POST['phone'];
$suburb = $_POST['suburb'];
$state = $_POST['state'];
$dealer = $_POST['dealer'];
$model = $_POST['model'];
$km = $_POST['km'];
$comments = $_POST['comments'];
$captcha= $_POST['g-recaptcha-response'];

$to      = $Email;

$subject = 'Service Request';

$message = "<html><body>";
$message .= "<p>Thank you for your Service request. Our Customer Care Team will be in contact with you shortly. Please keep this email for your record.</p>" . "<p>Here is the copy of the request you submitted to Moto Guzzi Australia.</p>";
$message .= "Date: " . $date . "<br>" ."<br>";
$message .= '<table>';
$message .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
$message .= "<tr><td><strong>Email:</strong> </td><td>" . $Email . "</td></tr>";
$message .= "<tr><td><strong>Phone:</strong> </td><td>" . $phone . "</td></tr>";
$message .= "<tr><td><strong>Suburb:</strong> </td><td>" . $suburb . "</td></tr>";
$message .= "<tr><td><strong>State:</strong> </td><td>" . $state . "</td></tr>";
$message .= "<tr><td><strong>Dealer:</strong> </td><td>" . $dealer . "</td></tr>";
$message .= "<tr><td><strong>Model:</strong> </td><td>" . $model . "</td></tr>";
$message .= "<tr><td><strong>KM:</strong> </td><td>" . $km . "</td></tr>";
$message .= "<tr><td><strong>Comments:</strong> </td><td>" . $comments . "</td></tr>";
$message .= "</table>";
$message .= "</body></html>";
$message .= "<p><br/><br/>Kind Regards, <br/>Moto Guzzi Australia</p>";



// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: JSA <agurung@jsg.com.au>' . "\r\n" . 'Reply-To: ' . $Email . "\r\n" . 'BCC: agurung@jsg.com.au';
			
					
// Storing in myphpAdmin Databse

if( !empty($name) and !empty($Email) and !empty($phone) and !empty($suburb) and !empty($state) and !empty($dealer) and !empty($model) and !empty($km) and !empty($captcha)){
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf4ARMTAAAAABPHnDPGO89AEmTfFXsTqJ2LkEjh&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	
	mail($to, $subject, $message, $headers);
	
	header("Location: success_service.php");
		
} 

else{
	header("Location: error_service.php");	
}
mysql_close();
?>