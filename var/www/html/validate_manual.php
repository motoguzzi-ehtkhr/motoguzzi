<?php


$date = date("d/m/Y");
$name = $_POST['name'];
$Email = $_POST['Email'];
$phone = $_POST['phone'];
$add = $_POST['add'];
$vin = $_POST['vin'];
$captcha= $_POST['g-recaptcha-response'];

$to      = $Email;

$subject = 'Owners Manual Request';

$message = "<html><body>";
$message .= "<p>This is a copy of the Owners Manual request you have submitted to Moto Guzzi Australia.</p>" . "<p>Our Customer Care Team will be in contact with you shortly. Please keep this email for your record.</p>";
$message .= "Date: " . $date . "<br>" ."<br>";
$message .= '<table>';
$message .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
$message .= "<tr><td><strong>Email:</strong> </td><td>" . $Email . "</td></tr>";
$message .= "<tr><td><strong>Phone:</strong> </td><td>" . $phone . "</td></tr>";
$message .= "<tr><td><strong>Address:</strong> </td><td>" . $add . "</td></tr>";
$message .= "<tr><td><strong>Vin No.:</strong> </td><td>" . $vin . "</td></tr>";
$message .= "</table>";
$message .= "</body></html>";
$message .= "<p><br/><br/>Kind Regards, <br/>Moto Guzzi Australia</p>";


// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: John Sample Automotive <customercare@motoguzzi.com.au>' . "\r\n" . 'Reply-To: ' . $Email . "\r\n" . 'BCC: customercare@motoguzzi.com.au';
			
					
// Storing in myphpAdmin Databse

if( !empty($name) and !empty($Email) and !empty($phone) and !empty($add) and !empty($vin) and !empty($captcha)){
	$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf4ARMTAAAAABPHnDPGO89AEmTfFXsTqJ2LkEjh&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
	
	mail($to, $subject, $message, $headers);
	
	header("Location: success_manual.php");
		
} 

else{
	header("Location: error_manual.php");	
}
mysql_close();
?>