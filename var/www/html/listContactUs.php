<!doctype html>

<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Moto Guzzi Australia</title>

<link rel="stylesheet" href="css/jquery.maximage.css" type="text/css" media="screen" title="CSS" charset="utf-8" />
<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/styles.css" />
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>

<body data-spy="scroll" data-target="#mynavbar" data-offset="0">
<header>
    <nav class="navbar navbar-default firstnavbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainnavbar">
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                        </button>
                <a href="index.html"><img src="images/motoguzzilogo.jpg" alt="Moto Guzzi Australia" /></a>
            </div>

            <div class="collapse navbar-collapse" id="mainnavbar">
                <ul class="nav navbar-nav text-uppercase">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">models <b class="caret"></b></a>
                        <ul class="dropdown-menu container" role="menu">
                            <li class="col-sm-2 col-xs-6 text-center"><a href="cali-touring-se.html"><img class="img-responsive" src="images/models-cali-touring-se-small.jpg" alt="California 1400 Touring SE"><p>California 1400 Touring SE</p></a></li>
                            <!--<li class="col-sm-2 col-xs-6 text-center"><a href="cali-touring.html"><img class="img-responsive" src="images/models-cali-touring-small.jpg" alt="California 1400 Touring"><p>California 1400 Touring</p></a></li>-->
                            <li class="col-sm-2 col-xs-6 text-center"><a href="cali-eldorado.html"><img class="img-responsive" src="images/models-cali-eldorado-small.jpg" alt="California Eldorado"><p>California Eldorado</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="cali-audace.html"><img class="img-responsive" src="images/models-cali-audace-small.jpg" alt="California Audace"><p>California Audace</p></a></li>
                            <!--<li class="col-sm-2 col-xs-6 text-center"><a href="cali-custom.html"><img class="img-responsive" src="images/models-cali-custom-small.jpg" alt="California 1400 Custom"><p>California 1400 Custom</p></a></li>-->
                            <!--<li class="col-sm-2 col-xs-6 text-center"><a href="stelvio-ntx.html"><img class="img-responsive" src="images/models-stelvio-ntx-small.jpg" alt="Stelvio 1200 NTX 8V"><p>Stelvio 1200 NTX 8V</p></a></li>-->
                            <li class="col-sm-2 col-xs-6 text-center"><a href="griso-se.html"><img class="img-responsive" src="images/models-griso-se-red-small.jpg" alt="Griso 1200 8V SE"><p>Griso 1200 8V SE</p></a></li>
                            <!--<li class="col-sm-2 col-xs-6 text-center"><a href="1200-sport.html"><img class="img-responsive" src="images/models-1200-sport-small.jpg" alt="1200 Sport 8V"><p>1200 Sport 8V</p></a></li>-->
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v9-roamer.html"><img class="img-responsive" src="images/models-v9-roamer-small.jpg" alt="V9 Roamer"><p>V9 Roamer</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v9-bobber.html"><img class="img-responsive" src="images/models-v9-bobber-small.jpg" alt="V9 Bobber"><p>V9 Bobber</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v7-II-racer.html"><img class="img-responsive" src="images/models-v7-II-racer-small.jpg" alt="V9 Roamer"><p>V7 II Racer</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v7-II-special.html"><img class="img-responsive" src="images/models-v7-special-2-small.jpg" alt="V7 II special"><p>V7 II special</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v7-II-stone.html"><img class="img-responsive" src="images/models-v7-stone-2-small.jpg" alt="V7 II stone"><p>V7 II stone</p></a></li>
                            <li class="col-sm-2 col-xs-6 text-center"><a href="v7-II-stornello.html"><img class="img-responsive" src="images/models-v7-II-stornello.jpg" alt="V9 Bobber"><p>V7 II stornello LE</p></a></li>
                        </ul>
                    </li>
                    <li> <!-- <a href="http://www.motoguzzi.com.au/accessories/">Accessories</a>--> </li>
                    <!--<li><a href="news.html">News</a></li>-->
                    <!--<li><a href="promotions.html">Promotions</a></li>-->
                </ul>

                <ul class="nav navbar-nav text-uppercase navbar-right">
                    <li><a href="ridereq.php">Contact Us</a></li>
                    <li><a href="dealers.html">Find a dealer</a></li>
                    <li><!-- <a href="http://www.motoguzzi.com.au/shop/">shop online</a> --></li>
                </ul>
            </div>
                </div><!--End of container-->
        </nav>     </header>

<?php
if(!isset($_POST["username"]) || empty($_POST["username"])){
?>
<p>Please Login </p>
<form id="form1" name="form1" method="post">
<p>
  <label for="textfield">Username:</label>
  <input style="border:1px solid #ff0000" type="text" name="username" id="username">
</p>
<p>
  <label for="password">Password:</label>
  <input style="border:1px solid #ff0000" type="password" name="password" id="password">
</p>
<input type="submit" name="submit" id="submit" value="Submit">
</form>
<p>&nbsp;</p>
<?php } else {
        $password=md5($_POST['password']);
        $file = fopen("/var/www/password/users.txt", "r") or die("Unable to open file!");
        $logonSuccessful=0;
        while(!feof($file)) {
        $line = fgets($file);
                if(preg_replace('/\s+/', '', $_POST['username'].$password)==preg_replace('/\s+/', '', $line)){
                        $logonSuccessful=1;
                }
    }
    fclose($file);
}
if($logonSuccessful==1){
?>
 <table width="110%" border="1">
  <tbody>
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone</th>
      <th scope="col">Suburb</th>
      <th scope="col">State</th>
      <th scope="col">Model</th>
      <th scope="col">VIN</th>
      <th scope="col">Dealer</th>
      <th scope="col">Comments</th>
    </tr>
<?php
        define ('DB_NAME', 'motoguz1_rideDB');
        define ('DB_USER', 'motoguz1_rider');
        define ('DB_PASSWORD', 'testride2016');
        define ('DB_HOST', 'localhost');
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "SELECT reg_date, name, Email, phone, suburb, state, model, vin, dealer, comments FROM testride ORDER BY reg_date DESC;";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                        echo "    <tr>\n";
                        echo "      <td>" . $row["reg_date"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["name"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["Email"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["phone"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["suburb"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["state"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["model"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["vin"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["dealer"]. "&nbsp;</td>\n";
                        echo "      <td>" . $row["comments"]. "&nbsp;</td>\n";
                        echo "    </tr>\n";
                }
        } else {
    echo "ERROR: 0 results";
        }
$conn->close();

        ?>
  </tbody>
</table>
<?php }elseif(isset($_POST["username"]) || !empty($_POST["username"])){
                        ?>
                        Logon failed, go back
<?php
        $to      = "itsupport@peterstevens.com.au";
        $subject = 'Failed login attempt to '.$_SERVER['HTTP_REFERER'];
        $message = "<html><body>";
        $message .= "<p>There was a failed login attempt on the Aprilia database list</p>";
        $message .= "<br /> This may just a user error or a valid hacking attempt.";
        $message .= "<br /> The username that attempted to login was: ".$_POST["username"];
        $message .= "<br /> With the password: ".$_POST["password"];
        $message .= "</body></html>";
        $message .= "<p><br/><br/>Kind Regards, <br/>Moto Guzzi Australia</p>";

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        //$headers .= 'Reply-To: ' . $Email;
        $headers .= 'From: PS Importers - Moto Guzzi Australia <motoguzzi@peterstevens.com.au>' . "\r\n" . 'Reply-To: itsupport@peterstevens.com.au' . "\r\n" .'BCC: itsupport@peterstevens.com.au';
echo "$headers";
        mail($to, $subject, $message, $headers);
}
        ?>

</body>
</html>


